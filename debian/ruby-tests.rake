require 'gem2deb/rake/spectask'

Gem2Deb::Rake::RSpecTask.new do |spec|
      spec.pattern = './spec/**/*_spec.rb'
      spec.exclude_pattern = '**/acceptance/puppet_lint_spec.rb'
end

